﻿Public Class EntityChangerV01

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub LeftToRight1_Click(sender As Object, e As EventArgs) Handles LeftToRight1.Click
        If ValidateData(TextBox1) = True Then
            CalcValue(TextBox1, TextBox2, 1)
        End If
    End Sub

    Private Sub RightToLeft1_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If ValidateData(TextBox2) = True Then
            CalcValue(TextBox2, TextBox1, 2)
        End If
    End Sub

    Private Sub LeftToRight2_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If ValidateData(TextBox4) = True Then
            CalcValue(TextBox4, TextBox3, 3)
        End If
    End Sub

    Private Sub RightToLeft2_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ValidateData(TextBox3) = True Then
            CalcValue(TextBox3, TextBox4, 4)
        End If
    End Sub

    Private Sub LeftToRight3_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If ValidateData(TextBox6) = True Then
            CalcValue(TextBox6, TextBox5, 5)
        End If
    End Sub

    Private Sub RightToLeft3_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If ValidateData(TextBox5) = True Then
            CalcValue(TextBox5, TextBox6, 6)
        End If
    End Sub

    Private Sub LeftToRight4_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If ValidateData(TextBox8) = True Then
            CalcValue(TextBox8, TextBox7, 7)
        End If
    End Sub

    Private Sub RightToLeft4_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If ValidateData(TextBox7) = True Then
            CalcValue(TextBox7, TextBox8, 8)
        End If
    End Sub

    Private Sub LeftToRight5_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If ValidateData(TextBox10) = True Then
            CalcValue(TextBox10, TextBox9, 9)
        End If
    End Sub

    Private Sub RightToLeft5_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If ValidateData(TextBox9) = True Then
            CalcValue(TextBox9, TextBox10, 8)
        End If
    End Sub

    Private Function ValidateData(obj) As Boolean
        If obj.Text = "" Then
            MsgBox("Empty box!", vbExclamation, "EntityChanger")
            Return False
        ElseIf Not IsNumeric(obj.Text) Then
            MsgBox("Required only numbers!", vbExclamation, "EntityChanger")
            Return False
        End If
        Return True
    End Function

    Private Sub CalcValue(obj1, obj2, Operation)
        Dim value As Double

        Select Case Operation
            Case 1
                'kabel na metr
                value = CDbl(Val(obj1.Text))
                value = value * 185.2
                obj2.Text = value
            Case 2
                'metr na kabel
                value = CDbl(Val(obj1.Text))
                value = value \ 185.2
                obj2.Text = value
            Case 3
                'mila morska na metr
                value = CDbl(Val(obj1.Text))
                value = value * 1852
                obj2.Text = value
            Case 4
                'metr na mila morska
                value = CDbl(Val(obj1.Text))
                value = value \ 1852
                obj2.Text = value
            Case 5
                'liga morska na metr
                value = CDbl(Val(obj1.Text))
                value = value * 5556
                obj2.Text = value
            Case 6
                'metr na liga morska
                value = CDbl(Val(obj1.Text))
                value = value \ 5556
                obj2.Text = value
            Case 7
                'cal na metr
                value = CDbl(Val(obj1.Text))
                value = value * 0.0253995
                obj2.Text = value
            Case 8
                'metr na cal
                value = CDbl(Val(obj1.Text))
                value = value / 0.0253995
                obj2.Text = value
            Case 9
                'stopa na metr
                value = CDbl(Val(obj1.Text))
                value = value * 0.304794
                obj2.Text = value
            Case 10
                'metr na stopa
                value = CDbl(Val(obj1.Text))
                value = value / 0.304794
                obj2.Text = value
            Case Else
                MsgBox("Invalid number of operation!")

        End Select
    End Sub

End Class
